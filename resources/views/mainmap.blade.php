@extends('app')
@section('style')
@endsection
@section('search')
<google-search></google-search>
@endsection
@section('sidebar')
<google-menu></google-menu>
<google-detail></google-detail>
@endsection
@section('content')
<!-- <div id="google-map" class="full-width full-height"></div> -->
<google-map name="google"></google-map>
@endsection
@section('script')
<!-- <script>
      var map;

      function initMap() {

        map = new google.maps.Map(document.getElementById('google-map'), {
          center: {lat: -6.889872, lng: 107.610781},
          zoom: 17,
          // mapTypeId: 'terrain',
          styles : 
          [
            {
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#f5f5f5"
                }
              ]
            },
            {
              "elementType": "labels",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "elementType": "labels.icon",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#616161"
                }
              ]
            },
            {
              "elementType": "labels.text.stroke",
              "stylers": [
                {
                  "color": "#f5f5f5"
                }
              ]
            },
            {
              "featureType": "administrative",
              "elementType": "geometry",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "administrative.land_parcel",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "administrative.land_parcel",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#bdbdbd"
                }
              ]
            },
            {
              "featureType": "administrative.neighborhood",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "poi",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "poi",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#eeeeee"
                }
              ]
            },
            {
              "featureType": "poi",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#757575"
                }
              ]
            },
            {
              "featureType": "poi.park",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#e5e5e5"
                }
              ]
            },
            {
              "featureType": "poi.park",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            },
            {
              "featureType": "road",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#ffffff"
                }
              ]
            },
            {
              "featureType": "road",
              "elementType": "labels.icon",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "road.arterial",
              "elementType": "labels",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "road.arterial",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#757575"
                }
              ]
            },
            {
              "featureType": "road.highway",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#dadada"
                }
              ]
            },
            {
              "featureType": "road.highway",
              "elementType": "labels",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "road.highway",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#616161"
                }
              ]
            },
            {
              "featureType": "road.local",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "road.local",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            },
            {
              "featureType": "transit",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "transit.line",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#e5e5e5"
                }
              ]
            },
            {
              "featureType": "transit.station",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#eeeeee"
                }
              ]
            },
            {
              "featureType": "water",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#c9c9c9"
                }
              ]
            },
            {
              "featureType": "water",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            }
          ], 
          mapTypeControl: false
        });

        var data_layer = new google.maps.Data({map: map});
        var data_layer_2 = new google.maps.Data({map: map});
        var data_layer_3 = new google.maps.Data({map: map});
        var data_layer_4 = new google.maps.Data({map: map});
        var data_layer_5 = new google.maps.Data({map: map});
        var data_layer_6 = new google.maps.Data({map: map});
        var data_layer_7 = new google.maps.Data({map: map});
        var data_layer_8 = new google.maps.Data({map: map});
        var data_layer_9 = new google.maps.Data({map: map});
        // map.mapTypes.set('OSM', osmMapType);

        // map.setMapTypeId('OSM');
        // map.setOptions({styles: styles['hide']});

        data_layer.loadGeoJson('https://campusgis-svc.test/informasispasial/bangunan');
        data_layer_2.loadGeoJson('https://campusgis-svc.test/informasispasial/jalan');
        // data_layer_3.loadGeoJson('https://campusgis-svc.test/informasispasial/kolam');
        data_layer_4.loadGeoJson('https://campusgis-svc.test/informasispasial/kolamsaraga');
        // data_layer_5.loadGeoJson('https://campusgis-svc.test/informasispasial/gardu');
        // data_layer_6.loadGeoJson('https://campusgis-svc.test/informasispasial/koridor');
        // data_layer_7.loadGeoJson('https://campusgis-svc.test/informasispasial/parkiran');
        data_layer_8.loadGeoJson('https://campusgis-svc.test/informasispasial/taman');
        data_layer_9.loadGeoJson('https://campusgis-svc.test/informasispasial/lapangan');

        data_layer.setStyle({
          fillColor: 'yellow',
          strokeWeight: 1
        });

        data_layer_2.setStyle({
          fillColor: 'grey',
          strokeWeight: 1
        });

        data_layer_6.setStyle({
          fillColor: 'red',
          strokeWeight: 1
        });

        data_layer_7.setStyle({
          fillColor: 'blue',
          strokeWeight: 1
        });

        data_layer_8.setStyle({
          fillColor: 'green',
          strokeWeight: 1
        });

        data_layer_9.setStyle({
          fillColor: 'darkgreen',
          strokeWeight: 1
        });


      };

</script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3jIbTHjV8RhVJwDdEoVLbPaSkjbu_O-c&libraries=geometry,places"></script>
<script src="js/app.js"></script>
@endsection