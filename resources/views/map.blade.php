<!DOCTYPE html>
<html>
  <head>
    <title>Campus GIS ITB</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }

      .map-control {
        background-color: #fff;
        border: 1px solid #ccc;
        box-shadow: 0 2px 2px rgba(33, 33, 33, 0.4);
        font-family: 'Roboto','sans-serif';
        margin: 10px;
        padding-right: 5px;
        /* Hide the control initially, to prevent it from appearing
           before the map loads. */
        display: none;
      }
      /* Display the control once it is inside the map. */
      #map .map-control { display: block; }

      .selector-control {
        font-size: 14px;
        line-height: 30px;
        vertical-align: baseline;
      }

    </style>
  </head>
  <body>
    <div id="map"></div>
    <script>
      var map;

      

      function initMap() {

        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -6.889872, lng: 107.610781},
          zoom: 17,
          // mapTypeId: 'terrain',
          styles : 
          [
            {
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#f5f5f5"
                }
              ]
            },
            {
              "elementType": "labels",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "elementType": "labels.icon",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#616161"
                }
              ]
            },
            {
              "elementType": "labels.text.stroke",
              "stylers": [
                {
                  "color": "#f5f5f5"
                }
              ]
            },
            {
              "featureType": "administrative",
              "elementType": "geometry",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "administrative.land_parcel",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "administrative.land_parcel",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#bdbdbd"
                }
              ]
            },
            {
              "featureType": "administrative.neighborhood",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "poi",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "poi",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#eeeeee"
                }
              ]
            },
            {
              "featureType": "poi",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#757575"
                }
              ]
            },
            {
              "featureType": "poi.park",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#e5e5e5"
                }
              ]
            },
            {
              "featureType": "poi.park",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            },
            {
              "featureType": "road",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#ffffff"
                }
              ]
            },
            {
              "featureType": "road",
              "elementType": "labels.icon",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "road.arterial",
              "elementType": "labels",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "road.arterial",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#757575"
                }
              ]
            },
            {
              "featureType": "road.highway",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#dadada"
                }
              ]
            },
            {
              "featureType": "road.highway",
              "elementType": "labels",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "road.highway",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#616161"
                }
              ]
            },
            {
              "featureType": "road.local",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "road.local",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            },
            {
              "featureType": "transit",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "transit.line",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#e5e5e5"
                }
              ]
            },
            {
              "featureType": "transit.station",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#eeeeee"
                }
              ]
            },
            {
              "featureType": "water",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#c9c9c9"
                }
              ]
            },
            {
              "featureType": "water",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            }
          ], 
          mapTypeControl: false
        });

        var data_layer = new google.maps.Data({map: map});
        // var data_layer_2 = new google.maps.Data({map: map});
        // var data_layer_3 = new google.maps.Data({map: map});
        // var data_layer_4 = new google.maps.Data({map: map});
        // var data_layer_5 = new google.maps.Data({map: map});
        // var data_layer_6 = new google.maps.Data({map: map});
        // var data_layer_7 = new google.maps.Data({map: map});
        // var data_layer_8 = new google.maps.Data({map: map});
        // var data_layer_9 = new google.maps.Data({map: map});
        // map.mapTypes.set('OSM', osmMapType);

        // map.setMapTypeId('OSM');
        // map.setOptions({styles: styles['hide']});

        data_layer.loadGeoJson(
            'http://campusgis-svc.test/informasispasial/bangunan');
        // data_layer_2.loadGeoJson(
        //     'http://campusgis-svc.test/informasispasial/jalan');
        // data_layer_3.loadGeoJson(
        //     'http://campusgis-svc.test/informasispasial/kolam');
        // data_layer_4.loadGeoJson(
        //     'http://campusgis-svc.test/informasispasial/kolamsaraga');
        // data_layer_5.loadGeoJson(
        //     'http://campusgis-svc.test/informasispasial/gardu');
        // data_layer_6.loadGeoJson(
        //     'http://campusgis-svc.test/informasispasial/koridor');
        // data_layer_7.loadGeoJson(
        //     'http://campusgis-svc.test/informasispasial/parkiran');
        // data_layer_8.loadGeoJson(
        //     'http://campusgis-svc.test/informasispasial/taman');
        // data_layer_9.loadGeoJson(
        //     'http://campusgis-svc.test/informasispasial/lapangan');

        data_layer.setStyle({
          fillColor: 'yellow',
          strokeWeight: 1
        });

        // data_layer_2.setStyle({
        //   fillColor: 'grey',
        //   strokeWeight: 1
        // });

        // data_layer_6.setStyle({
        //   fillColor: 'red',
        //   strokeWeight: 1
        // });

        // data_layer_7.setStyle({
        //   fillColor: 'blue',
        //   strokeWeight: 1
        // });

        // data_layer_8.setStyle({
        //   fillColor: 'green',
        //   strokeWeight: 1
        // });

        // data_layer_9.setStyle({
        //   fillColor: 'darkgreen',
        //   strokeWeight: 1
        // });

        data_layer.addListener('click', function(event) {
          console.log(event);
        });
      };

      

      // var styles = {
      //   default: null,
      //   hide: [
      //     {
      //       featureType: 'poi.business',
      //       stylers: [{visibility: 'off'}]
      //     },
      //     {
      //       featureType: 'transit',
      //       elementType: 'labels.icon',
      //       stylers: [{visibility: 'off'}]
      //     }
      //   ]
      // };

      // var osmMapType = new google.maps.ImageMapType({
        //   getTileUrl: function(coord, zoom) {
        //     return "http://tile.openstreetmap.org/" + zoom +
        //     "/" + coord.x + "/" + coord.y + ".png";
        //     },
        //     tileSize: new google.maps.Size(256, 256),
        //     name: "OpenStreetMap",
        //     maxZoom: 18
        // });

        // var mapOptions = {
        //    center: new google.maps.LatLng(39.9078, 32.8252),
        //    zoom: 10,
        //    {mapTypeIds: [google.maps.MapTypeId.ROADMAP,
        //    'OSM']}
        // };

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3jIbTHjV8RhVJwDdEoVLbPaSkjbu_O-c&callback=initMap"
    async defer></script>

  </body>
</html>