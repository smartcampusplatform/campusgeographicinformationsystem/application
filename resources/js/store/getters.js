    let getters = {
         posts: state => {
             return state.posts
         },
         points: state => {
             return state.points
         },
         datapoint: state => {
             return state.datapoint
         },
         points_parkiran: state => {
             return state.points_parkiran
         },
         points_evacuation: state => {
             return state.points_evacuation
         },
         points_eventkampus: state => {
             return state.points_eventkampus
         },
         points_shuttlebus: state => {
             return state.points_shuttlebus
         },
         points_poolbus: state => {
             return state.points_poolbus
         }

    }

    export default  getters