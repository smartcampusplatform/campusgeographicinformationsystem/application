let actions = {
        // createPost({commit}, post) {
        //     axios.post('/api/posts', post)
        //         .then(res => {
        //             commit('CREATE_POST', res.data)
        //         }).catch(err => {
        //         console.log(err)
        //     })

        // },
        // fetchPosts({commit}) {
        //     axios.get('/api/posts')
        //         .then(res => {
        //             commit('FETCH_POSTS', res.data)
        //         }).catch(err => {
        //         console.log(err)
        //     })
        // },
        // deletePost({commit}, post) {
        //     axios.delete(`/api/posts/${post.id}`)
        //         .then(res => {
        //             if (res.data === 'ok')
        //                 commit('DELETE_POST', post)
        //         }).catch(err => {
        //         console.log(err)
        //     })
        // },
        searchPoints({commit}, searchText) {
            axios.get('http://campusgis-svc.test/informasispasial/bangunan', {
                params: {
                    search: searchText
            }})
                .then(res => {
                    commit('FETCH_POINTS', res.data.data)
                }).catch(err => {
                console.log(err)
            })
        },
        getPoint({commit}, searchText) {
            axios.get('http://campusgis-svc.test/informasispasial/bangunan', {
                params: {
                    id: searchText
            }})
                .then(res => {
                    commit('GET_POINT', res.data.data)
                }).catch(err => {
                console.log(err)
            })
        },
        getParkiran({commit}, searchText) {
            axios.get('http://campusgis-svc.test/informasispasial/parkiran', {
                params: {
                    search: searchText
            }})
                .then(res => {
                    commit('GET_PARKIRAN', res.data.data)
                }).catch(err => {
                console.log(err)
            })
        },
        simulateParkiran({commit}) {
            axios.get('http://campusgis-svc.test/simulasi/parkiran')
                .then(res => {
                    commit('GET_PARKIRAN', res.data.data)
                }).catch(err => {
                console.log(err)
            })
        },
        getEvacuationArea({commit}, searchText) {
            axios.get('http://campusgis-svc.test/informasispasial/evacuationarea', {
                params: {
                    search: searchText
            }})
                .then(res => {
                    commit('GET_EVACUATION', res.data.data)
                }).catch(err => {
                console.log(err)
            })
        },
        getEventKampus({commit}) {
            axios.get('http://campusgis-svc.test/personalisasi/eventkampus')
                .then(res => {
                    commit('GET_EVENTKAMPUS', res.data.data)
                }).catch(err => {
                console.log(err)
            })
        },
        getShuttleBus({commit}) {
            axios.get('http://campusgis-svc.test/personalisasi/shuttlebus')
                .then(res => {
                    commit('GET_SHUTTLEBUS', res.data.data)
                }).catch(err => {
                console.log(err)
            })
        },
        getPoolBus({commit}) {
            axios.get('http://campusgis-svc.test/personalisasi/poolbus')
                .then(res => {
                    commit('GET_POOLBUS', res.data.data)
                }).catch(err => {
                console.log(err)
            })
        }
    }

    export default actions