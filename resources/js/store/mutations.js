let mutations = {
        // CREATE_POST(state, post) {
        //     state.posts.unshift(post)
        // },
        // FETCH_POSTS(state, posts) {
        //     return state.posts = posts
        // },
        // DELETE_POST(state, post) {
        //     let index = state.posts.findIndex(item => item.id === post.id)
        //     state.posts.splice(index, 1)
        // },
        FETCH_POINTS(state, points) {
            return state.points = points
        },
        GET_POINT(state, datapoint) {
            return state.datapoint = datapoint
        },
        GET_PARKIRAN(state, points_parkiran) {
            return state.points_parkiran = points_parkiran
        },
        GET_EVACUATION(state, points_evacuation) {
            return state.points_evacuation = points_evacuation
        },
        GET_EVENTKAMPUS(state, points_eventkampus) {
            return state.points_eventkampus = points_eventkampus
        },
        GET_SHUTTLEBUS(state, points_shuttlebus) {
            return state.points_shuttlebus = points_shuttlebus
        },
        GET_POOLBUS(state, points_poolbus) {
            return state.points_poolbus = points_poolbus
        },

    }
    export default mutations